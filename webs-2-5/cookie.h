//############################
//####### cookie.h ###########
//############################
#ifndef _h_WS_COOKIE
#define _h_WS_COOKIE 1

#ifdef __cplusplus
extern"C"{
#endif

/* set cookie in path with name, value and expire time */
void ws_set_cookie(webs_t wp, char *name, char *value, int min, char *path, 
                        char *pcOthersInTail);

/* clear cookie in 'path' named 'name' */
void ws_clear_cookie(webs_t wp, char *name, char *path);

/* get cookie with specific name */
int ws_get_cookie(webs_t wp, char *name, char **ppValue, int *pLen);
int zhou_get_cookie(webs_t wp, char**cookie, int *len);    
void zhou_set_cookie(webs_t wp,char* str);
#ifdef __cplusplus
}
#endif

#endif
