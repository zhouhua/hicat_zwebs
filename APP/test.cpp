/*
auth:9crk
feedback:admin@9crk.com
20160307 Shenzhen China
all rights reserved.
*/
#include<stdio.h>
#include<signal.h>
#include<string.h>
#include<stdlib.h>
#include<unistd.h>
#include <time.h>
#include<dirent.h>

extern "C"{
	extern int init_zwebs(int port);
	extern int zwebsRegister(char*name, void* funcProc);
	extern int exit_zwebs();
}
int getVal(char* query,char* name,char* val)
{
    char* p = query;
    char* pp = val;
    int len;
    len = strlen(name);
    while(*p != '\0'){
        if(strncmp(p,name,len) == 0){
            p+=len+1;
            while(*p != '&' && *p != '\0'){
                *pp = *p;
                pp++;
                p++;
            }
            *pp = '\0';
            return 0;
        }
        p++;
    }
    return -1;
}
void startRecord(char*query,char* echo)
{
	char cmdBuff[100];
	char buff[30];
	int iSave;
	if(access("/www/mmc/video",0) != 0){
		sprintf(echo,"{\"result\":\"ERROR\"}");
		return;
	}
	if(getVal(query,"save",buff) == 0){
		iSave = atoi(buff);
		if(iSave){
			system("touch /dev/savevideo");
			sprintf(echo,"{\"result\":\"OK\"}");
		}else{
			system("rm /dev/savevideo");
			sprintf(echo,"{\"result\":\"OK\"}");
		}
	}else if(getVal(query,"del",buff) == 0){
		//DO SOME SECURITY CHECK!!!!
		sprintf(cmdBuff,"rm /www/mmc/video/%s",buff);	
		system(cmdBuff);
		sprintf(echo,"{\"result\":\"OK\"}");
	}else{
		sprintf(echo,"{\"result\":\"ERROR\"}");
	}
	return;
}
void updateFileList(char* query,char* echo)
{
	struct dirent *dp;
 	DIR *dirp;	
	char buff[20000];
	if(access("/www/mmc/video",0) != 0){
        sprintf(echo,"{\"result\":\"ERROR\"}");
        return;
    }
	sprintf(buff,"{\"result\":\"OK\",\"files\":[");
	dirp = opendir("/www/mmc/video/");
	if((dp = readdir(dirp)) != NULL){
		sprintf(buff,"%s{\"name\":\"%s\"}",buff,dp->d_name);
		while ((dp = readdir(dirp)) != NULL){	
        	if(strlen(dp->d_name) == 1 || strlen(dp->d_name) == 2)continue;
			//printf("name = %s len = %d\n",dp->d_name,strlen(dp->d_name));
			sprintf(buff,"%s,{\"name\":\"%s\"}",buff,dp->d_name);
		}
	}
	sprintf(buff,"%s]}",buff);
	closedir(dirp);
	sprintf(echo,"%s",buff);
}
void setTime(char* query,char* echo)
{
	time_t timep;
	char cmdBuff[100];
	int year,month,day,hour,min,sec;
	sscanf(query,"time=%d-%d-%d-%d-%d-%d",&year,&month,&day,&hour,&min,&sec);
	//printf("==%s==\n",query);	
	sprintf(cmdBuff,"date --set \"%d-%d-%d %d:%d:%d\"",year,month+1,day,hour,min,sec);
	system(cmdBuff);
	time(&timep);
	sprintf(echo,"{\"result\":\"OK\"}");
	//sprintf(echo,"current time is:%s\n",asctime(gmtime(&timep)));
}
void apMode(char*query,char* echo)
{
	char cmdBuff[100];	
	char SSID[50];
	char pass[50];
	int ret1,ret2;
	ret1 = getVal(query,"ssid",SSID);
	ret2 = getVal(query,"password",pass);	
	if(ret1 == 0 && ret2 == 0){
		sprintf(cmdBuff,"/hicat/launch.sh ap %s %s",SSID,pass);
		system(cmdBuff);	
	}else{
		sprintf(cmdBuff,"/hicat/launch.sh ap hicat 88888888");
		system(cmdBuff);	
	}
}
void stationMode(char*query,char* echo)
{
	char cmdBuff[100];	
	char SSID[50];
	char pass[50];
	int ret1,ret2;
	ret1 = getVal(query,"ssid",SSID);
	ret2 = getVal(query,"password",pass);	
	if(ret1 == 0 && ret2 == 0){
		sprintf(cmdBuff,"/hicat/launch.sh sta %s %s",SSID,pass);
		system(cmdBuff);	
	}else{
		//sprintf(cmdBuff,"/www/mmc/hicat/launch.sh scan");
		//system(cmdBuff);
		sprintf(echo,"{\"result\":\"ERROR\"}");
		//sprintf(echo,"<script>document.location=\"/listhead.htm\"</script>");
	}
}
void setCamera(char*query,char* echo)
{
	char cmdBuff[100];
	char buff1[20];
	char buff2[20];
	char buff3[20];
	int ret1,ret2,ret3;
	ret1 = getVal(query,"resolve",buff1);	
	ret2 = getVal(query,"rotate",buff2);	
	ret3 = getVal(query,"fps",buff3);	
	if(ret1==0){
		sprintf(cmdBuff,"sed -i 's/^RESOLVE.*/RESOLVE= %s/' /hicat/video.conf",buff1);		
		system(cmdBuff);
	}
	if(ret2==0){
		sprintf(cmdBuff,"sed -i 's/^ROTATE.*/ROTATE= %s/' /hicat/video.conf",buff2);		
		system(cmdBuff);
	}
	if(ret3==0){
		sprintf(cmdBuff,"sed -i 's/^FPS.*/FPS= %s/' /hicat/video.conf",buff3);		
		system(cmdBuff);
	}
	
	if(ret1 == 0 && ret2 == 0){	
		sprintf(cmdBuff,"/hicat/launch.sh rtsp");
		system(cmdBuff);
		sprintf(echo,"{\"result\":\"OK\"}");		
		//sprintf(echo,"set camera param to %s %s success",buff1,buff2);
	}else{
		sprintf(echo,"{\"result\":\"ERROR\"}");
		//sprintf(echo,"error");
	}
	
}
void snapshot(char*query,char* echo)
{
	char cmdbuf[500];
	int ret1,ret2;
	char buff1[200];
	char buff2[200];
	memset(buff1,0,200);
	memset(buff2,0,200);
	ret1 = getVal(query,"dir",buff1);	
	ret2 = getVal(query,"name",buff2);
	if(ret1 == 0 && ret2 == 0){
		sprintf(cmdbuf,"echo \"%s%s\">/dev/snapctl",buff1,buff2);
		system(cmdbuf);
		sprintf(echo,"{\"result\":\"OK\"}");
	}else{
		sprintf(echo,"{\"result\":\"ERROR\"}");	
	}
}
void switchVideoServer(char*query,char* echo)
{
	int ret1;
	int mode;
	char buff1[200];
	memset(buff1,0,200);
	ret1 = getVal(query,"mode",buff1);	
	if(ret1 == 0){
		mode = atoi(buff1);
		if(mode == 1){
			system("/hicat/launch.sh guard stop");
			system("killall rtspserver");
			system("sed 's/rtspserver/libhisiv_test/g' -i /hicat/video.conf");
			system("/hicat/launch.sh guard start");	
		}else{
			system("/hicat/launch.sh guard stop");
			system("killall libhisiv_test");
			system("sed 's/libhisiv_test/rtspserver/g' -i /hicat/video.conf");
			system("/hicat/launch.sh guard start");	
		}
		sprintf(echo,"{\"result\":\"OK\"}");
	}else{
		sprintf(echo,"{\"result\":\"ERROR\"}");	
	}
}
void test(char*query,char* echo)
{
	sprintf(echo,"{\"result\":\"OK\"}");
}
/*******************************************
main
*******************************************/
int main(int argc, char* argv[])
{
	init_zwebs(80);
	zwebsRegister("stationMode",(void*)stationMode);
	zwebsRegister("apMode",(void*)apMode);
	zwebsRegister("setTime",(void*)setTime);
	zwebsRegister("files",(void*)updateFileList);
	zwebsRegister("record",(void*)startRecord);
	zwebsRegister("test",(void*)test);
	zwebsRegister("setCamera",(void*)setCamera);
	zwebsRegister("snapshot",(void*)snapshot);	
	zwebsRegister("videoServer",(void*)switchVideoServer);	
	while(1)sleep(10);
	exit_zwebs();
}
